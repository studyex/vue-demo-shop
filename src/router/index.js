import Vue from 'vue'
import Router from 'vue-router'
import IndexPage from '@/views/Index'
import DetailPage from '@/views/product/Detail'
import DetailCount from '@/views/product/detail/count'
import DetailAnalysis from '@/views/product/detail/analysis'
import DetailForecast from '@/views/product/detail/forecast'
import DetailPublish from '@/views/product/detail/publish'
import OrderList from '@/views/product/OrderList'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Index',
      component: IndexPage
    },
    {
      path: '/detail',
      name: 'Detail',
      component: DetailPage,
      redirect: '/detail/count', //跳转到count
      children: [
        {
          path: 'count',
          component: DetailCount
        }, {
          path: 'analysis',
          component: DetailAnalysis
        }, {
          path: 'forecast',
          component: DetailForecast
        }, {
          path: 'publish',
          component: DetailPublish
        }
      ]
    },
    {
      name:'OrderList',
      path:'/orderList',
      component:OrderList
    }
  ]
})
