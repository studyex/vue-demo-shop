import Mock from 'mockjs'
Mock.mock(/getNewsList/, {
  'list|10': [
    // {
    //   title: 'title1',
    //   url: 'xxx'
    // },
    {
      title: '@ctitle(5,20)',
      url: '@url'
    }
  ]
});
Mock.mock(/getBoardList/, [
  {
    id: "car",
    title: "开放产品",
    description: "开放产品是一款开放产品",
    saleout: false,
    toKey: '#/detail/count'
  },
  {
    id: "earth",
    title: "品牌营销",
    description: "品牌营销帮助你的产品更好低找到定位",
    saleout: false,
    toKey: '#/detail/analysis'
  },
  {
    id: "loud",
    title: "使命必达",
    description: "使命必达快速迭代永远保持最前端的速度",
    saleout: false,
    toKey: '#/detail/forecast'
  },
  {
    id: "hill",
    title: "勇攀高峰",
    description: "帮你勇闯高峰，到达事业的顶峰",
    toKey: "#/detail/publish",
    saleout: false
  }
]
);
function getRImg() {
  return Mock.Random.image('700x300', Mock.Random.hex())
};
Mock.mock(/getSlides/, [
  {
    ky: 1,
    title: '@ctitle(4,12)',
    src: getRImg(),
    toKey: 'analysis'
  },
  {
    ky: 2,
    title: '@ctitle(4,12)',
    src: getRImg(),
    toKey: 'count'
  },
  {
    ky: 3,
    title: '@ctitle(4,12)',
    src: getRImg(),
    toKey: 'publish'
  },
  {
    ky: 4,
    title: '@ctitle(4,12)',
    src: getRImg(),
    toKey: 'forecast'
  },
]);
//------------------------------>
Mock.mock(/userLogin/, {
  body: {
    'userName': '@word(5,10)'
  },
  msg: '登录成功！',
  state: 200
});




//------------------------------>
Mock.mock(/getPrice/, {
  'amount|1-100': 100
});
Mock.mock(/createOrder/,
  'orderId|1-100'
);
Mock.mock(/checkOrder/,
  'orderId|1-100'
);
Mock.mock(/getOrderList/, {
  'list|10': [
    {
      'orderId|10000-100000': 1,
      'product|1': ["数据统计", "数据预测", "流量分析", "广告发布"],
      'period|1': ['一年', '半年', '两年', '三年'],
      'version|1': ['客户版', '代理商版', '专家版'],
      'buyTypes|1': ["入门版", "中级版", "高级版"],
      'buyNum|1-10': 2,
      'date': '@date("yyyy-MM-dd")',
      'amount|1-10000': 1
    }
  ]
});
