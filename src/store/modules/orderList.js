import Axios from "axios";

const state = {
    orderList: [],
    params: {}
}
const getters = {
    getOrderList: (state) => {
        return state.orderList;
    }
}
const actions = {
    fetchOrderList({ commit, state }) {
        Axios.post('/api/getOrderList', state.params).then(res => {
            commit('updateOrderList', res.data.list);
            state.orderList = res.data.list;
            //state.total = res.date.total;
        }, error => {
            console.error(error);
        })
    }
}
const mutations = {
    updateOrderList(state, payload) {
        state.orderList = payload;
    },
    updateParams(state, {key,val}) {
        state.params[key] = val;
    },
}
export default {
    state,
    getters,
    actions,
    mutations
}