import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import orderList from './modules/orderList'

Vue.use(Vuex);

export default new Vuex.Store({
    modules:{
        orderList
    }
})